---
layout: handbook-page-toc
title: "LATAM Region Handbook"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##  LATAM Region Mission

**Work in progress**

## Who are we

| Team Member Name             | Role                      | Segment            | Territory Name | Countries                                                                                                                                                                                                                                                                                                  |
| ---------------- | ------------------------- | ------------------ | -------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Carlos Dominguez | Strategic Account Leader  | Enterprise         | LATAM North    | Venezuela,Turks and Caicos Islands, Trinidad and Tobago, Saint Vincent and the Grenadines, Saint Martin, Saint Lucia, Saint Kitts and Nevis, Puerto Rico, Peru, Panama, Nicaragua, Montserrat, Mexico. Martinique, Jamaica, Honduras, Haiti, Guyana, Guadeloupe, Grenada, French Guiana, Ecuador, Colombia |
| Jim Torres       | Strategic Account Leader  | Enterprise         | LATAM South    | Uruguay, Suriname. Paraguay ,Guatemala, Falkland Islands, El Salvador, Dominican Republic, Dominica, Curacao, Cuba ,Costa Rica, Chile, Cayman Islands, British Virgin Islands, Brazil, Bolivia, Bermuda, Belize, Barbados, Bahamas, Aruba, Argentina, Antigua and Barbuda, Anguilla.                       |
| Romer Gonzalez   | Account Executive         | Mid-Market and SMB | LATAM          | All                                                                                                                                                                                                                                                                                                        |
| Bruno Lazzarin   | SDR                       | Mid-Market and SMB | LATAM          | All                                                                                                                                                                                                                                                                                                        |
| Leo Vieira       | SDR                       | Enterprise         | LATAM          | All                                                                                                                                                                                                                                                                                                        |
| Hugo Azevedo     | Solution Architect        | Enterprise         | LATAM          | All                                                                                                                                                                                                                                                                                                        |
| Ricardo Amarilla | Technical Account Manager | Enterprise         | LATAM          | All                                                                                                                                                                                                                                                                                                        |
