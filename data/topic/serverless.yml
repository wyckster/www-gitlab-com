description: Serverless software architecture uses cloud managed services and
  event driven code, allowing developers to build scalable and cost-efficient
  applications
canonical_path: /topics/serverless/
file_name: serverless
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: What is serverless?
header_body: >-
  Serverless is a software architecture design pattern that takes advantage of
  event-driven code execution powered by cloud managed services to build
  massively scalable and cost-efficient applications composed of small discrete
  functions without developers needing to design for or think about the
  underlying infrastructure where their code runs.


  [Learn more about GitLab →](https://about.gitlab.com/why/)
cover_image: /images/topics/serverless-header.svg
body: >-
  ## Serverless business logic


  What is serverless? Every application uses servers at some point. The term Serverless emphasizes an architecture and service model where the developers need not concern themselves with infrastructure and instead can focus on the business logic of their appliction. Serverless is the next evolution of architectural design from monolith, to [microservices](/topics/microservices/), to functions as Adrian Cockcroft explains in this video:


  <iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/aBcG57Gw9k0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


  ## Serverless, FaaS (Functions as a service), and managed services


  Often serverless and FaaS are treated as interchangeable terms, but this isn't really accurate. Serverless is an overarching architectural pattern that makes use of a FaaS along with other cloud managed services. FaaS is a specific type of service such as AWS Lambda, Google Cloud Functions, and Azure Functions, that enables developers to deploy functions.


  ## Attributes of serverless


  1. Small, discrete units of code. Often services written using serverless architecture are comprised of a single function.

  2. Event-based execution. The infrastructure needed to run a function doesn't exist until a function is triggered. Once an event is received an ephemeral compute environment is created to execute that request. The environment may be destroyed immediately, or more commonly stays active for a short period of time, commonly 5 minutes.

  3. Scale to zero. Once a function stops receiving requests the infrastructure is taken down and completely stops running. This saves on cost since the infrastructure only runs when there is usage. If there's no usage, the environment scales down to zero.

  4. Scale to infinity. The FaaS takes care of monitoring load and creating additional instances when needed, in theory, up to infinity. This virtually eliminates the need for developers to think about scale as they design applications. A single deployed function can handle one or one billion requests without any change to the code.

  5. Use of managed services. Often serverless architectures make use of cloud provided services for elements of their application that provide non-differentiated heavy lifting such as file storage, databases, queueing, etc. For example, Google's Firebase is popular in the serverless community as a database and state management service that connects to other Google services like Cloud Functions.


  ## Comparison of cloud managed services


  Here is a chart of with examples of managed services from AWS, Google Cloud, and Azure along with their open source counterparts.


  | Service       | Open Source                 | AWS          | Google Cloud    | Azure                     |

  | ------------- | --------------------------- | ------------ | --------------- | ------------------------- |

  | FaaS          | Knative                     | Lambda       | Cloud Functions | Azure Functions           |

  | Storage       | Minio                       | S3           | Cloud storage   | Azure Storage             |

  | SQL DB        | MySQL                       | RDS          | Cloud SQL       | Azure SQL Database        |

  | NoSQL DB      | MongoDB, Cassandra, CouchDB | DynamoDB     | Cloud Datastore | Cosmos DB                 |

  | Message queue | Kafka, Redis, RabbitMQ      | SQS, Kinesis | Google Pub/Sub  | Azure Queue Storage       |

  | Service mesh  | Istio                       | App Mesh     | Istio on GKE    | Azure Service Fabric Mesh |
benefits_title: Business Values and Benefits of GitLab Serverless
benefits_description: "[GitLab
  Serverless](https://about.gitlab.com/stages-devops-lifecycle/serverless/)
  allows business to deploy their own FaaS on Kubernetes."
benefits:
  - title: Faster
    description: Faster pace of innovation. Developer productivity increases when
      they can focus solely on business logic.
    image: /images/icons/gitlab-rocket.svg
  - description: Greater stability/resiliency (less loss of revenue due to downtime).
    title: Stability
    image: /images/icons/stable-computer.svg
  - description: Greater scale, the software is able to keep up with business demand.
    title: Scale
    image: /images/icons/scale.svg
  - description: Lower costs. Since compute is only billed when a service is active,
      servless provides tremendous cost savings vs always-on infrastructure.
    title: Cost
    image: /images/icons/piggy-bank.svg
  - title: No vendor lock-in
    description: No vendor lock-in. Organizations can choose who they want to run
      their compute. In any cloud that supports Kubernetes, or even on-premises
      servers.
    image: /images/icons/cloud.svg
  - description: Your FaaS is part of the same workflow as the rest of your software
      lifecyle with a single appliction from planning and testing, to deployment
      and monitoring.
    title: Workflow
    image: /images/icons/continuous-integration.svg
  - description: Deploying functions is greatly streamlined and simplified vs using
      Knative directly.
    title: Deployment
    image: /images/icons/gitlab-pipeline.svg
resources_title: Additional resources
resources:
  - title: What is Serverless Architecture? What are its Pros and Cons?
    url: https://hackernoon.com/what-is-serverless-architecture-what-are-its-pros-and-cons-cc4b804022e9
    type: Blog
  - title: Knative
    url: https://cloud.google.com/knative/
    type: Blog
  - title: Martin Folwer on serverless architectures
    url: https://martinfowler.com/articles/serverless.html
    type: Blog
suggested_content:
  - url: /blog/2019/11/19/gitlab-serverless-with-cloudrun-for-anthos/
  - url: /blog/2019/09/12/is-serverless-the-end-of-ops/
  - url: /blog/2020/04/29/aws-gitlab-serverless-webcast/
